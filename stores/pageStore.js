import pages from '~/server/pageData'

export const usePageStore = defineStore('page', {
  state: () => ({
    page: {},
  }),
  
  actions: {
    async show(slug) {
      
      const page = pages.find(
        (page) => page.slug === slug
      );
      
      this.page = page
    }
  },
  getters: {},
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(usePageStore, import.meta.hot));
}